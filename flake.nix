{
  description = "Jekkyl Flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        package = "mccartyio";
        packages = with pkgs; [
          gems
          bundler
          ruby
          bundix
          nodePackages.prettier
          sass
          scss-lint
          python39Packages.md2gemini
        ];
        gems = pkgs.bundlerEnv {
          ruby = pkgs.ruby;
          name = "gemset";
          gemfile = ./Gemfile;
          lockfile = ./Gemfile.lock;
          gemset = ./gemset.nix;
          groups = [ "default" "production" "development" "test" "jekyll_plugins"];
        };
      in
      {
        packages.${package} = pkgs.stdenv.mkDerivation {
          pname = package;
          version = "1.0";
          src = ./.;
          buildInputs = packages;
          buildPhase = ''
            JEKYLL_ENV=production jekyll build --destination $out/public
          '';
          installPhase = "true";
        };

        defaultPackage = self.packages.${system}."${package}";

        devShell =
          pkgs.mkShell {
            buildInputs = packages;

          };
      });
}
