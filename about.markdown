---
layout: page
title: About
permalink: /about/
---

My name is Nathan, and I'm a systems engineer with the good fortune to be able to work in rust on a
daily basis.

I work in, and quite enjoy, distributed systems. I also have deep interests in cryptography,
authenticated data structures, archival, and paranoid security

# My Projects

## Rust

- [Asuran](https://sr.ht/~thatonelutenist/Asuran/): High security _and_ high performance deduplicating archiver
  - [Replicator](https://git.sr.ht/~thatonelutenist/replicator): A basic framework for building
    applications making use of encrypted, authenticated storage, using XChaCha20+Blake3 and
    balloon hashing
- [Redacted](https://git.sr.ht/~thatonelutenist/redacted): Wrapper for potentially sensitive
  bytestrings to control formatting
- [Snapper](https://gitlab.com/rust-community-matrix/snapper): Very WIP Matrix bot framework
- [PeerDex](https://git.sr.ht/~thatonelutenist/peerdex): Peer-to-Peer search engine (Very WIP)

## Idris 2

- [idris2-libs](https://sr.ht/~thatonelutenist/idris2-libs/): General libraries for Idris 2
  - [rope](https://git.sr.ht/~thatonelutenist/idris2-rope): Rope data structure for Idris 2

## Nix

- [System Flake](https://git.sr.ht/~thatonelutenist/system-flake): My NixOS system configuration, including
  my emacs configuration.

## Web

- [Community.rs](https://www.community.rs/): Matrix homeserver for the rust community ([Element Web
  Instance](https://element.community.rs/))

# [Dotfiles](https://sr.ht/~thatonelutenist/dotfiles/)

I am currently in the process of migrating to
[home-manager](https://github.com/nix-community/home-manager) and [Doom
Emacs](https://github.com/doomemacs/doomemacs) so the following configurations
may be a little incomplete, but I am using them daily and they _most_ of my
configuration in them currently:

- [Nix Configuration Flake](https://git.sr.ht/~thatonelutenist/system-flake) (includes
  home manager)
- [Doom Emacs Personal Directory](https://git.sr.ht/~thatonelutenist/doom.d)

# Contact Info

Feel free to reach out to me via the following means:

- Matrix: [@nathan:mccarty.io](https://matrix.to/#/@nathan:mccarty.io)
- Keybase: [thatonlutenist](https://keybase.io/thatonelutenist)

# Interested in hiring me?

I am currently interested in hearing about new opportunities, my resume is available here:
([pdf](https://mccarty.io/statics/cv-no-phone.pdf)).

If you are interested in reaching out, please get in touch via the email in my resume.

# Crypto

GPG Key: [B7A40A5D78C08885](https://mccarty.io/statics/nathan.asc)
