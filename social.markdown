---
layout: page
title: Social
permalink: /social/
---

# Social Media

Im on on social media at the following handles:

- Mastodon: [@thatonelutenist@im-in.space](https://im-in.space/@thatonelutenist)

# Other Contact Info

Feel free to reach out to me via the following means:

- Matrix: [@nathan:mccarty.io](https://matrix.to/#/@nathan:mccarty.io)
- Keybase: [thatonlutenist](https://keybase.io/thatonelutenist)
