---
layout: post
title: "The State of Data Dirs"
date: 2020-05-18
---
As the [issue has come up](https://github.com/xdg-rs/dirs/issues/21) on
how to handle data dirs on macOS in the dirs-next project, I thought it
might be prudent to take a look at how other popular libraries in other
languages deal with the issue.

There is some contention over whether XDG style directories (e.g.
`~/.config/`{.verbatim}) or macOS default style directories should be
used (e.g. `~/Library/Application Support`{.verbatim}).

# What do other languages do?

## Python - [appdirs](https://pypi.org/project/appdirs/)

Uses macOS default paths, actively maintained, in the 360 most popular
python packages. Does *not* respect XDG dirs environment variables on
macOS

## Javascript/NodeJS - [xdg-basedir](https://www.npmjs.com/package/xdg-basedir)

Seems to just always fall back to XDG dirs, without attempting to use
macOS default paths. Doesn\'t even support windows paths. It will,
however, respect the XDG environment variables if set.

There is a smaller [appdirs](https://www.npmjs.com/package/appdirs) that
is a direct port of the python version, with the same behavior as the
python version, but nobody seems to use it (\~7,000,000 vs 102 weekly
downloads)

## Golang - [xdg](https://github.com/adrg/xdg)

Defaults to macOS default directories, but appears to respect XDG
variables when set.

There is also the slightly more popular BurntSushi
[xdg](https://godoc.org/github.com/BurntSushi/xdg), which seems to also
default to XDG paths.

## Java - [appdirs](https://github.com/harawata/appdirs)

Not super popular, but most popular one I could find. Defaults to macOS
default directories, does not seem to respect XDG variables on macOS.

## Ruby - [xdg](https://rubyworks.github.io/xdg/)

Seems to always follow the XDG standard.

# Neat Summary Table

Summary of behavior on macOS:

``` example
|----------+----------+------------+------------------|
| Language | XDG Dirs | macOS Dirs | Respects XDG Env |
|----------+----------+------------+------------------|
| Python   |          | x          | no               |
|----------+----------+------------+------------------|
| NodeJS   | x        |            | yes              |
|----------+----------+------------+------------------|
| Golang   |          | x          | yes              |
|----------+----------+------------+------------------|
| Java     |          | x          | no               |
|----------+----------+------------+------------------|
| Ruby     | x        |            | yes              |
|----------+----------+------------+------------------|
```

# Personal Experience

In my experience, most of the command line applications I have on my mac
(which I essentially only use for testing my software on macOS). All of
my applications appear to be using either XDG base directories, or
traditional `~/.appname`{.verbatim} folders.

# Current status of `directories`

Currently, `directories` is returning `~/Library/Preferences`{.verbatim}
as the config directory. This is, at best, incorrect, as the apple
documentation (and inspecting the current state of my mac) indicate that
this directory should only be used for macOS managed plists.

Unfortunately, by macOS standards, the config directory
(`~/Library/Application Support`{.verbatim}) happens to be the same as
the data directory, so \'fixing\' this bug may cause issues with
dependent code assuming that the data and config directories are
separate folders, when on macOS, they will not be. This, at the very
least, needs a callout and some examples in the documentation/readme.

# Conclusion

In my opinion, keeping things in line with the apple way, the correct
path moving forward is as follows:

-   Correct the config directory to point to
    `~/Library/Application Support`{.verbatim} by default
-   Provide a feature gate to re-enable the old, broken behavior, to
    ease migration.
-   Respect the user\'s XDG environment variables, using the directories
    pointed to by them instead of the defaults, even on macOS.
-   Provide a feature gate to default to XDG base dirs, even on macOS.
